import React from "react";
import { StyledTop } from "./styles";
import { Button, Search, Logo } from "../";

export const Top = ({ theme }) => {
  return (
    <StyledTop>
      <Logo />
      <Search />
      <Button color="#3DB46D" text="Add a photo" />
    </StyledTop>
  );
};
