import styled from "styled-components";

export const StyledTop = styled.div`
  display: grid;
  grid-template-columns: 20% 30% 50%;
  top: 0;
  height: 7rem;
  width: 100%;
  align-items: center;
`;
