import React from "react";
import { StyledButton } from "./styles";

export const Button = ({ color, text }) => {
  return <StyledButton color={color}> {text}</StyledButton>;
};
