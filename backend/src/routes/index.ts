import { Router } from "express";
import { getImages, addImage } from "../services/images.service";

const router: Router = Router();

router.post("/new-image", addImage);
router.get("/images", getImages);

export default router;
