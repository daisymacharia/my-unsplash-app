import { Response, Request } from "express";
import { Image, Images } from "../types/images.interface";
import ImageModel from "../models/images";

const getImages = async (req: Request, res: Response) => {
  try {
    const images: Image[] = await ImageModel.find();
    res.status(200).json({ images });
  } catch (error) {
    throw error;
  }
};

const addImage = async (req: Request, res: Response) => {
  try {
    const body = req.body as Pick<Image, "label" | "photo_url" | "description">;
    console.log(body, res);

    const image: Image = new ImageModel({
      label: body.label,
      photo_url: body.photo_url,
      description: body.description,
    });

    const newImage: Image = await image.save();
    const allImages: Image[] = await ImageModel.find();
    res
      .status(201)
      .json({ message: "Todo added", image: newImage, images: allImages });
  } catch (error) {
    throw error;
  }
};
export { getImages, addImage };
