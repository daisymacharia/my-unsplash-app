import React from "react";
import { StyledSearch, SearchContainer } from "./styles";
import { AiOutlineSearch } from "react-icons/ai";

export const Search = () => {
  return (
    <SearchContainer>
      <AiOutlineSearch />
      <StyledSearch placeholder="Search by name" />
    </SearchContainer>
  );
};
