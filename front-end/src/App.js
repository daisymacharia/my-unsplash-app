import React from "react";
// import { useContext } from "react";
import { ThemeProvider } from "styled-components";
import { theme } from "./palette";
import { MainPage } from "./containers";

// const ThemeContext = React.createContext(theme);

function App() {
  return (
    <ThemeProvider theme={theme}>
      <MainPage />
    </ThemeProvider>
  );
}

export default App;
