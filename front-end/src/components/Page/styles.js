import styled from "styled-components";

export const StyledPage = styled.div`
  background: ${(props) => props.theme.background};
  height: 100vh;
  width: 100vw;
  text-align: center;
  padding: 0 2rem;
`;
