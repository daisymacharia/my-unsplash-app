import React from "react";
import { FaUser } from "react-icons/fa";
import { StyledLogo, Heading, SubHeading } from "./styles";

export const Logo = () => {
  return (
    <StyledLogo>
      <FaUser />
      <div>
        <Heading>My Unsplash</Heading>
        <SubHeading>devchallenges.io</SubHeading>
      </div>
    </StyledLogo>
  );
};
