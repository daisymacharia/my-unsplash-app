export const theme = {
  background: "#ffffff",
  darkBg: "#333333",
  radius: "24px",
  lightGray: "#bdbdbd",
  shadow: "0px 1px 6px rgba(0, 0, 0, 0.1)",
  green: "#3DB46D",
};
