import React from "react";
import { Page, Top } from "../components";
import PhotosGrid from "./PhotosGrid";

const MainPage = () => {
  return (
    <Page>
      <Top></Top>
      <PhotosGrid />
    </Page>
  );
};

export default MainPage;
