import * as dotenv from "dotenv";
import mongoose from "mongoose";
import express, { Express } from "express";
import cors from "cors";
import bodyParser from "body-parser";
import imageRoutes from "./routes";

dotenv.config();

if (!process.env.PORT) {
  process.exit(1);
}

const PORT: number = parseInt(process.env.PORT as string, 10);

const options = { useNewUrlParser: true, useUnifiedTopology: true };
mongoose.set("useFindAndModify", false);
const uri = "mongodb://localhost/image-gallery";

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(imageRoutes);

app.get("/", (req, res) => {
  res.send("Welcome to photo gallery app");
});

mongoose
  .connect(uri, options)
  .then(() =>
    app.listen(PORT, () =>
      // tslint:disable-next-line:no-console
      console.log(`Server running on http://localhost:${PORT}`)
    )
  )
  .catch((error) => {
    throw error;
  });
