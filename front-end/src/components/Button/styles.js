import styled from "styled-components";

export const StyledButton = styled.button`
  grid-area: 1/3/-1/-1;
  width: 6rem;
  height: 2.5rem;
  justify-self: end;
  background: ${(props) => props.color};
  box-shadow: ${(props) => props.theme.shadow};
  border-radius: ${(props) => props.theme.radius};
  border: none;
  font-family: Noto Sans;
  font-style: normal;
  font-weight: bold;
  font-size: 0.7rem;
  line-height: 19px;
  color: #ffffff;
`;
