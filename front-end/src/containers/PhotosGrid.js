import React, { useState, useEffect, useRef } from "react";
import {
  AutoSizer,
  CellMeasurer,
  CellMeasurerCache,
  createMasonryCellPositioner,
  Masonry,
} from "react-virtualized";
import axios from "axios";
import ImageMeasurer from "react-virtualized-image-measurer";
import { fetchImages } from "../utils/fetchImages";

const keyMapper = (item, index) => item.source || index;

const columnWidth = 200;
const defaultHeight = 250;
const defaultWidth = columnWidth;

const cache = new CellMeasurerCache({
  defaultHeight,
  defaultWidth,
  fixedWidth: true,
});

const cellPositioner = createMasonryCellPositioner({
  cellMeasurerCache: cache,
  columnCount: 4,
  columnWidth: 200,
  spacer: 10,
});

const MasonryComponent = ({ itemsWithSizes, setRef }) => {
  const cellRenderer = ({ index, key, parent, style }) => {
    const { item, size } = itemsWithSizes[index];
    const height = columnWidth * (size.height / size.width) || defaultHeight;
    return (
      <CellMeasurer cache={cache} index={index} key={key} parent={parent}>
        <div style={style}>
          {item.urls.regular && (
            <img
              src={item.urls.regular}
              alt={item.alt_description}
              style={{
                height: height,
                width: columnWidth,
                display: "block",
                borderRadius: "10px",
              }}
            />
          )}
        </div>
      </CellMeasurer>
    );
  };

  return (
    <Masonry
      cellCount={itemsWithSizes.length}
      cellMeasurerCache={cache}
      cellPositioner={cellPositioner}
      cellRenderer={cellRenderer}
      height={600}
      width={900}
      keyMapper={keyMapper}
      ref={setRef}
    />
  );
};

const PhotosGrid = () => {
  let masonryRef = useRef(null);
  const [list, setList] = useState([]);

  useEffect(() => {
    axios
      .get(
        "https://api.unsplash.com/photos/?client_id=" +
          "s-deZ4xeKeGDH-Mrsc9ojGXyBthA6f_jyDMe342PPV4"
      )
      .then((data) => {
        return setList(data.data);
      })
      .catch((err) => {
        console.log("Error happened during fetching!", err);
      });
  });

  const setMasonry = (node) => (masonryRef.current = node);

  return (
    list.length && (
      <ImageMeasurer
        items={list}
        image={(item) => item.urls.regular}
        keyMapper={keyMapper}
        onError={(error, item, src) => {
          console.error(
            "Cannot load image",
            src,
            "for item",
            item,
            "error",
            error
          );
        }}
        defaultHeight={defaultHeight}
        defaultWidth={defaultWidth}
      >
        {({ itemsWithSizes }) => (
          <MasonryComponent
            setRef={setMasonry}
            itemsWithSizes={itemsWithSizes}
          />
        )}
      </ImageMeasurer>
    )
  );
};

export default PhotosGrid;
