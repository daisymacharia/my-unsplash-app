import styled from "styled-components";

export const SearchContainer = styled.div`
  position: relative;
  color: ${(props) => props.theme.lightGray};
  svg {
    position: absolute;
    z-index: 2;
    top: 1rem;
    left: 2rem;
  }
`;

export const StyledSearch = styled.input`
  grid-area: 1/2;
  width: 15rem;
  height: 3rem;
  border: 1px solid #bdbdbd;
  filter: drop-shadow(0px 1px 6px rgba(0, 0, 0, 0.1));
  border-radius: ${(props) => props.theme.radius};
  padding: 0.5rem;
  outline: none;
  text-indent: 2rem;

  ::placeholder {
    position: relative;
    font-family: Noto Sans;
    font-style: normal;
    font-weight: 500;
    font-size: 0.9rem;
    line-height: 19px;
  }
`;
