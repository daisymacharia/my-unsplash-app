import { Image } from "../types/images.interface";
import { Schema, model } from "mongoose";
import { timeStamp } from "console";

const ImageSchema: Schema = new Schema({
  label: {
    type: String,
    required: true,
  },
  photo_url: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  date_created: {
    type: Date,
    default: Date.now,
  },
});

export default model<Image>("ImageModel", ImageSchema);
