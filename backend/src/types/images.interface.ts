import { Document } from "mongoose";

export interface Image extends Document {
  label: string;
  photo_url: string;
  description: string;
  date_created: string;
}

export interface Images {
  [key: number]: Image;
}
