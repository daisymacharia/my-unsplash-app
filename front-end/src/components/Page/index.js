import React from "react";
import { StyledPage } from "./styles";

export const Page = ({ children }) => {
  return <StyledPage>{children}</StyledPage>;
};
